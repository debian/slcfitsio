#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
PKG=slcfitsio
DIR=$PKG-$2
TAR=../${PKG}_$2+nosvn.orig.tar.gz

# clean up the upstream tarball
tar xfz $3
rm -rf $DIR/.svn
tar cfz $TAR $DIR
rm -rf $DIR
